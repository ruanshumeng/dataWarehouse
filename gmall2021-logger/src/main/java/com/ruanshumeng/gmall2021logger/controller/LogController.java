package com.ruanshumeng.gmall2021logger.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class LogController {

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    //    @PostMapping()
    @RequestMapping("applog")
    public String addLog(@RequestParam("param") String json) {

        // 落盘
        log.info(json);

        // 输出到kafka
        kafkaTemplate.send("ods_base_log", json);


        return "success";

    }

}
