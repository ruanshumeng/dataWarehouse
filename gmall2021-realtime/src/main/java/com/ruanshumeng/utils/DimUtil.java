package com.ruanshumeng.utils;

import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.common.GmallConfig;
import redis.clients.jedis.Jedis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.List;

/**
 * phoenix 专属查询
 */
public class DimUtil {

    public static JSONObject queryList(Connection connection, String tableName, String id) throws Exception {

        String key = tableName + ":" + id;
        // 由于hbase查询时间相对较为缓慢 改用redis做旁路缓存
        Jedis jedis = RedisUtil.getJedis();
        String strJson = jedis.get(key);
        if (strJson != null) {
            jedis.close();
            return JSONObject.parseObject(strJson);
        }


        String querySql = "select * from " + GmallConfig.HBASE_SCHEMA + "." + tableName + " where id = '" + id + "'";
        List<JSONObject> list = JDBCUtil.queryList(connection, querySql, JSONObject.class, true);

        JSONObject jsonObject = list.get(0);
        jedis.set(key, jsonObject.toJSONString());
        jedis.expire(key, 24 * 60 * 60);// 设置一天过期时间

        return jsonObject;
    }

    public static void deleteCached(String key){
        Jedis jedis = RedisUtil.getJedis();
        jedis.del(key);
        jedis.close();
    }

    public static void main(String[] args) throws Exception {
        Class.forName(GmallConfig.PHOENIX_DRIVER);
        Connection connection = DriverManager.getConnection(GmallConfig.PHOENIX_SERVER);
        long start = new Date().getTime();
        System.out.println(queryList(connection, "DIM_USER_INFO", "711"));
        long end = new Date().getTime();
        System.out.println(queryList(connection, "DIM_USER_INFO", "711"));
        long end2 = new Date().getTime();
        System.out.println(end - start);
        System.out.println(end2 - end);
    }

}
