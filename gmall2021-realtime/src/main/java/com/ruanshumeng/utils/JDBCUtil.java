package com.ruanshumeng.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.CaseFormat;
import com.ruanshumeng.common.GmallConfig;
import org.apache.commons.beanutils.BeanUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCUtil {

    /**
     * @param connection        连接
     * @param querysql          查询sql
     * @param clz
     * @param upderScoreToCamel 是否转驼峰
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> List<T> queryList(Connection connection,
                                        String querysql,
                                        Class<T> clz,
                                        Boolean upderScoreToCamel) throws Exception {
        List<T> list = new ArrayList<>();
        // 预编译
        PreparedStatement preparedStatement = connection.prepareStatement(querysql);
        // 执行
        ResultSet resultSet = preparedStatement.executeQuery();
        // 获取数量
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        while (resultSet.next()) {
            T t = clz.newInstance();

            // JDBC下标从1开始
            for (int i = 1; i < columnCount + 1; i++) {
                //  获取Name
                String columnName = metaData.getColumnName(i);
                // 获取列值
                Object object = resultSet.getObject(i);
                if (upderScoreToCamel) {
                    columnName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, columnName.toLowerCase());
                }
                // 赋值
                BeanUtils.setProperty(t, columnName, object);

            }
            list.add(t);
        }
        resultSet.close();
        preparedStatement.close();



        return list;
    }

    public static void main(String[] args) throws  Exception {
        Class.forName(GmallConfig.PHOENIX_DRIVER);
        Connection connection = DriverManager.getConnection(GmallConfig.PHOENIX_SERVER);
        List<JSONObject> list = queryList(connection,
                "select *from GMALL2021_REALTIME.DIM_USER_INFO",
                JSONObject.class,
                true);

        for (JSONObject jsonObject : list) {
            System.out.println(jsonObject);
        }

    }

}
