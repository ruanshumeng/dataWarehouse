package com.ruanshumeng.utils;

import com.ruanshumeng.common.GmallConfig;
import com.ruanshumeng.common.TransientSink;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.connector.jdbc.JdbcStatementBuilder;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ClickHouseUtil {

    public static <T> SinkFunction<T> getClickSink(String sql) {

        return JdbcSink.sink(sql, new JdbcStatementBuilder<T>() {
                    @Override
                    public void accept(PreparedStatement preparedStatement, T t) throws SQLException {
                        // 获取泛型参数
                        Field[] fields = t.getClass().getDeclaredFields();
                        try {
                            int offset = 0;
                            for (int i = 0; i < fields.length; i++) {
                                Field field = fields[i];
                                //设置私有属性可访问
                                field.setAccessible(true);
                                // 获取参数上的自定义注解
                                TransientSink annotation = field.getAnnotation(TransientSink.class);
                                if (annotation != null) {
                                    offset++;
                                    continue;
                                }

                                // 通过泛型参数获取值
                                Object o = field.get(t);
                                // sql下标从1开始
                                preparedStatement.setObject(i + 1 - offset, o);

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new JdbcExecutionOptions.Builder()
                        .withBatchSize(5)
                        .build(),
                new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                        .withDriverName(GmallConfig.CLICKHOUSE_DRIVER)
                        .withUrl(GmallConfig.CLICKHOUSE_URL)
                        .build());

    }

}
