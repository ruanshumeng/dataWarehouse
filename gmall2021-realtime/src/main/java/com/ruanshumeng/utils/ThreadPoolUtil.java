package com.ruanshumeng.utils;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolUtil {

    public static ThreadPoolExecutor pool;

    public static ThreadPoolExecutor getThreadPool() {

        if (pool == null) {
            synchronized (ThreadPoolUtil.class) {
                if (pool == null) {
                    pool = new ThreadPoolExecutor(
                            4, 6, 300L, TimeUnit.SECONDS,
                            new LinkedBlockingQueue<>()
                    );
                }
            }
        }

        return pool;
    }


}
