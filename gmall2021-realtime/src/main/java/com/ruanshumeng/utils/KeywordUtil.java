package com.ruanshumeng.utils;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 14:01 2022/1/18
 * @Modified By:
 */
public class KeywordUtil {
    public static List<String> analyze(String text) {
        List<String> resultList = new ArrayList<>();
        StringReader reader = new StringReader(text);
        IKSegmenter ikSegmenter = new IKSegmenter(reader, false);

        while (true) {
            try {
                Lexeme next = ikSegmenter.next();
                if (next != null) {
                    String lexemeText = next.getLexemeText();
                    resultList.add(lexemeText);
                } else {
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return resultList;
    }
}
