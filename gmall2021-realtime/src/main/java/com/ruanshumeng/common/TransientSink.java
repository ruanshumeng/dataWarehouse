package com.ruanshumeng.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 17:04 2021/12/29
 * @Modified By:
 */
@Target(ElementType.FIELD)  // 泛型作用域
@Retention(RUNTIME)
public @interface TransientSink {
}
