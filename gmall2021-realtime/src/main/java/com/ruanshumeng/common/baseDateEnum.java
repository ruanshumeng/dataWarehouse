package com.ruanshumeng.common;

public enum baseDateEnum {
    yearMS(31536000000L);

    private Long data;

    baseDateEnum(Long data) {
        this.data = data;
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }
}
