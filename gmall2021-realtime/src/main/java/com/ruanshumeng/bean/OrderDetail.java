package com.ruanshumeng.bean;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 10:46 2021/12/17
 * @Modified By:
 */
@Data
public class OrderDetail {
    Long id;
    Long order_id;
    Long sku_id;
    BigDecimal order_price;
    Long sku_num;
    String sku_name;
    String create_time;
    BigDecimal split_total_amount;
    BigDecimal split_activity_amount;
    BigDecimal split_coupon_amount;
    Long create_ts;
}
