package com.ruanshumeng.function;

import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.bean.TableProcess;
import com.ruanshumeng.common.GmallConfig;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class TableProcessFunction extends BroadcastProcessFunction<JSONObject, String, JSONObject> {

    private MapStateDescriptor<String, TableProcess> mapStateDescriptor;
    private OutputTag<JSONObject> outputTag;

    public TableProcessFunction() {
    }

    public TableProcessFunction(MapStateDescriptor<String, TableProcess> mapStateDescriptor, OutputTag<JSONObject> outputTag) {
        this.mapStateDescriptor = mapStateDescriptor;
        this.outputTag = outputTag;
    }

    private Connection connection;


    @Override
    public void open(Configuration parameters) throws Exception {
        Class.forName(GmallConfig.PHOENIX_DRIVER);
        connection = DriverManager.getConnection(GmallConfig.PHOENIX_SERVER);
    }


    @Override
    public void processBroadcastElement(String value, BroadcastProcessFunction<JSONObject, String, JSONObject>.Context ctx, Collector<JSONObject> out) throws Exception {


        // 1. 解析数据 解析成TableProcess
        JSONObject jsonObject = JSONObject.parseObject(value);
        String after = jsonObject.getString("after");
        TableProcess tableProcess = JSONObject.parseObject(after, TableProcess.class);
        // 防止写入hbase的数据在hbase上建表
        if (TableProcess.SINK_TYPE_HBASE.equals(tableProcess.getSinkType())) {
            // 2. 建表
            checkTable(tableProcess.getSinkTable(), tableProcess.getSinkColumns(), tableProcess.getSinkPk(), tableProcess.getSinkExtend());
        }
        // 3.存储状态 广播出去
        BroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(mapStateDescriptor);
        // 获取源表名 + 操作类型
        broadcastState.put(tableProcess.getSourceTable() + tableProcess.getOperateType(), tableProcess);


    }

    @Override
    public void processElement(JSONObject value, BroadcastProcessFunction<JSONObject, String, JSONObject>.ReadOnlyContext ctx, Collector<JSONObject> out) throws Exception {

        // 获取key
        String tableName = value.getString("tableName");
        String type = value.getString("type");
        String key = tableName + type;

        ReadOnlyBroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(mapStateDescriptor);
        TableProcess tableProcess = broadcastState.get(key);
        if (tableProcess != null) {
            String sinkColumns = tableProcess.getSinkColumns();
            List<String> list = Arrays.asList(sinkColumns.split(","));

            // 将目标信息插入到value中
            value.put("sinkTable", tableProcess.getSinkTable());
            JSONObject afterJson = value.getJSONObject("after");
            // 处理字段数据
            fliterColumns(afterJson, list);


            // 分流
            if (TableProcess.SINK_TYPE_HBASE.equals(tableProcess.getSinkType())) {
                ctx.output(outputTag, value);
            }
            if (TableProcess.SINK_TYPE_KAFKA.equals(tableProcess.getSinkType())) {
                out.collect(value);
            }


        } else {
            System.out.println("当前key不存在【" + key + "】");
        }


    }

    private void fliterColumns(JSONObject data, List<String> list) {


//        for (Map.Entry<String, Object> stringObjectEntry : data.entrySet()) {
//            String key = stringObjectEntry.getKey();
//            if(!list.contains(key)){
//                data.remove(key);
//            }
//        }
        Set<Map.Entry<String, Object>> entries = data.entrySet();

        Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> next = iterator.next();
            String key = next.getKey();
            if (!list.contains(key)) {
                iterator.remove();
            }
        }

//        // 简化写法
//        Set<Map.Entry<String, Object>> entries = data.entrySet();
//        entries.removeIf(x -> !list.contains(x.getKey()));


    }


    private void checkTable(String sinkTable, String sinkColumns, String sinkPk, String sinkExtend) {
        PreparedStatement preparedStatement = null;
        StringBuffer createTable = new StringBuffer().append("create table if not exists ")
                .append(GmallConfig.HBASE_SCHEMA).append('.')
                .append(sinkTable).append(" (");
        // 防止空值处理
        if (sinkPk == null) sinkPk = "id";
        if (sinkExtend == null) sinkExtend = "";
        // 组装参数
        String[] words = sinkColumns.split(",");
        for (int i = 0; i < words.length; i++) {
            if (sinkPk.equals(words[i])) {
                createTable.append(words[i]).append(" varchar primary key  ");
            } else {
                createTable.append(words[i]).append(" varchar ");
            }
            // 判断是否为最后一个参数
            if (i < words.length - 1) {
                createTable.append(" ,");
            }
        }
        createTable.append(" )").append(sinkExtend);
        try {
            preparedStatement = connection.prepareStatement(createTable.toString());
            preparedStatement.execute();
        } catch (SQLException e) {
            try {
                preparedStatement.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        System.out.println(createTable);


    }


}
