package com.ruanshumeng.function;

import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.common.GmallConfig;
import com.ruanshumeng.utils.DimUtil;
import com.ruanshumeng.utils.ThreadPoolUtil;
import lombok.SneakyThrows;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Collections;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 封装异步查询
 * 1. 如果使用同步查询的话 每个维度加载的时间相加 效率太低 故而改为异步查询的方式 多个维度可以同时查询phoenix的值
 *
 * @param <T>
 */
public abstract class DimAsyncFunction<T> extends RichAsyncFunction<T, T> implements DimJoinFunction<T> {
    private String tbName;

    public DimAsyncFunction(String tbName) {
        this.tbName = tbName;
    }

    private Connection connection;
    private ThreadPoolExecutor threadPoolExecutor;

    @Override
    public void open(Configuration parameters) throws Exception {
        Class.forName(GmallConfig.PHOENIX_DRIVER);
        connection = DriverManager.getConnection(GmallConfig.PHOENIX_SERVER);
        threadPoolExecutor = ThreadPoolUtil.getThreadPool();
    }

    @Override
    public void asyncInvoke(T input, ResultFuture<T> resultFuture) throws Exception {

        threadPoolExecutor.submit(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {

                // 获取数据 表名+“："+id  查询维度数据
                String key = getKey(input);
                JSONObject jsonObject = DimUtil.queryList(connection, tbName, key);
                // 组装
                join(input,jsonObject);
                // 返回
                resultFuture.complete(Collections.singletonList(input));
            }
        });



    }

    @Override
    public void timeout(T input, ResultFuture<T> resultFuture) throws Exception {
        resultFuture.complete(Collections.singletonList(input));
    }
}
