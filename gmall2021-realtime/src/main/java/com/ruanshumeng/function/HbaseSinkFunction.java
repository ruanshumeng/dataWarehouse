package com.ruanshumeng.function;

import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.common.GmallConfig;
import com.ruanshumeng.utils.DimUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Locale;
import java.util.Set;

public class HbaseSinkFunction extends RichSinkFunction<JSONObject> {

    Connection connection;

    @Override
    public void open(Configuration parameters) throws Exception {
        Class.forName(GmallConfig.PHOENIX_DRIVER);
        connection = DriverManager.getConnection(GmallConfig.PHOENIX_SERVER);
    }

    @Override
    public void invoke(JSONObject value, Context context) throws Exception {
        PreparedStatement preparedStatement = null;
        JSONObject afterJson = value.getJSONObject("after");
        String sinkTable = value.getString("sinkTable");
        String phoenixSql = createPhoeNixSql(sinkTable, afterJson);
        if ("update".equals(value.getString("type"))) {
            DimUtil.deleteCached(sinkTable.toUpperCase(Locale.ROOT) + ":" + afterJson.getString("id"));
        }
        System.out.println(phoenixSql);
        try {
            preparedStatement = connection.prepareStatement(phoenixSql);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            preparedStatement.close();
        }


    }

    private String createPhoeNixSql(String sinkTable, JSONObject after) {
        Set<String> keys = after.keySet();
        Collection<Object> values = after.values();

        return "upsert into " + GmallConfig.HBASE_SCHEMA + "." + sinkTable + "( " +
                StringUtils.join(keys, ",") + " ) values('" +
                StringUtils.join(values, "','") + "' )";

    }

}
