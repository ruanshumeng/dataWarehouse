package com.ruanshumeng.function;

import com.ruanshumeng.utils.KeywordUtil;
import org.apache.flink.table.annotation.DataTypeHint;
import org.apache.flink.table.annotation.FunctionHint;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

import java.util.List;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 14:38 2022/1/18
 * @Modified By:
 */
@FunctionHint(output = @DataTypeHint("ROW<s STRING>"))
public class KeywordFunction extends TableFunction<Row> {
    public void eval(String value) {
        List<String> keywordList = KeywordUtil.analyze(value);
        for (String keyword : keywordList) {
            Row row = new Row(1);
            row.setField(0, keyword);
            collect(row);
        }
    }
}
