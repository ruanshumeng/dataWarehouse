package com.ruanshumeng.app.dws;

import com.ruanshumeng.bean.ProvinceStats;
import com.ruanshumeng.utils.ClickHouseUtil;
import com.ruanshumeng.utils.MyKafkaUtils;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 17:14 2022/1/16
 * @Modified By:
 */
public class ProvinceStatsSqlApp {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        // 通过ddl获取kafka主题数据

        String groupId = "province_stats";
        String orderWideTopic = "dwm_order_wide";
        tableEnv.executeSql("CREATE TABLE ORDER_WIDE (" +
                "province_id BIGINT, " +
                "province_name STRING," +
                "province_area_code STRING," +
                "province_iso_code STRING," +
                "province_3166_2_code STRING," +
                "order_id STRING, " +
                "total_amount DOUBLE," +
                "create_time STRING," +
                "rt AS  TO_TIMESTAMP(create_time) ," +
                "WATERMARK FOR rt AS rowtime)" +
                " WITH (" + MyKafkaUtils.getKafkaDDL(orderWideTopic, groupId) + ")");
        // 分组开窗聚合

        Table provinceStateTable = tableEnv.sqlQuery("select " +
                "DATE_FORMAT(TUMBLE_START(rowtime, INTERVAL '10' SECOND ),'yyyy-MM-dd HH:mm:ss')   stt, " +
                "DATE_FORMAT(TUMBLE_END(rowtime, INTERVAL '10' SECOND ),'yyyy-MM-dd HH:mm:ss')  edt , " +
                " province_id,province_name,province_area_code," +
                "province_iso_code,province_3166_2_code," +
                "COUNT( DISTINCT order_id) order_count, sum(total_amount) order_amount," +
                "UNIX_TIMESTAMP()*1000 ts " +
                " from ORDER_WIDE group by TUMBLE(rowtime, INTERVAL '10' SECOND )," +
                " province_id,province_name,province_area_code,province_iso_code,province_3166_2_code ");
        DataStream<ProvinceStats> provinceStatsDataStream =
                tableEnv.toAppendStream(provinceStateTable, ProvinceStats.class);
        provinceStatsDataStream.addSink(ClickHouseUtil.getClickSink("insert into province_stats_2021 values(?,?,?,?,?,?,?,?,?,?)"));
        env.execute();

    }
}
