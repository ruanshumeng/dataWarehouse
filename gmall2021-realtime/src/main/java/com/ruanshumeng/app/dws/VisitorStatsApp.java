package com.ruanshumeng.app.dws;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPObject;
import com.ruanshumeng.bean.VisitorStats;
import com.ruanshumeng.utils.ClickHouseUtil;
import com.ruanshumeng.utils.MyKafkaUtils;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

/**
 * 访客主题宽表
 * 1.
 * 2.跳出
 * 3.pv and uv
 */
public class VisitorStatsApp {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 获取页面日志
        DataStreamSource<String> pageDs = env.addSource(MyKafkaUtils.getKafkaConsumer("dwd_page_log", "visitor_stats_app"));
        // uv
        DataStreamSource<String> uvDs = env.addSource(MyKafkaUtils.getKafkaConsumer("dwm_unique_visit", "visitor_stats_app"));
        // 跳出
        DataStreamSource<String> jumpDs = env.addSource(MyKafkaUtils.getKafkaConsumer("dwm_jump_detail", "visitor_stats_app"));

        // 将数据转化为相同的bean 方便后面进行union
        SingleOutputStreamOperator<VisitorStats> pageMapDs = pageDs.map(line -> {
            JSONObject json = JSON.parseObject(line);
            JSONObject commonJson = json.getJSONObject("common");
            JSONObject pageJSON = json.getJSONObject("page");
            // 进入页面数  如果上一次页面id不存在记1
            String pageId = pageJSON.getString("last_page_id");
            Long svCnt = 0L;
            if (pageId == null || 0 >= pageId.length()) {
                svCnt = 1L;
            }
            return new VisitorStats(
                    "",
                    "",
                    commonJson.getString("vc"),
                    commonJson.getString("ch"),
                    commonJson.getString("ar"),
                    commonJson.getString("is_new"),
                    0L, 1L, svCnt, 0L, pageJSON.getLong("during_time"), json.getLong("ts")

            );
        });

        SingleOutputStreamOperator<VisitorStats> uvMapDs = uvDs.map(line -> {
            JSONObject json = JSON.parseObject(line);
            JSONObject commonJson = json.getJSONObject("common");

            return new VisitorStats(
                    "",
                    "",
                    commonJson.getString("vc"),
                    commonJson.getString("ch"),
                    commonJson.getString("ar"),
                    commonJson.getString("is_new"),
                    1L, 0L, 0L, 0L, 0L, json.getLong("ts")

            );
        });

        SingleOutputStreamOperator<VisitorStats> jumpMapDs = jumpDs.map(line -> {
            JSONObject json = JSON.parseObject(line);
            JSONObject commonJson = json.getJSONObject("common");
            return new VisitorStats(
                    "",
                    "",
                    commonJson.getString("vc"),
                    commonJson.getString("ch"),
                    commonJson.getString("ar"),
                    commonJson.getString("is_new"),
                    0L, 0L, 0L, 1L, 0L, json.getLong("ts")

            );
        });

        DataStream<VisitorStats> unionDS = uvMapDs.union(pageMapDs, jumpMapDs);

        // 提取时间戳生成wk
        SingleOutputStreamOperator<VisitorStats> visitorStatsSingleOutputStreamOperator = unionDS.assignTimestampsAndWatermarks(WatermarkStrategy.<VisitorStats>forBoundedOutOfOrderness(
                Duration.ofSeconds(1)
        ).withTimestampAssigner(new SerializableTimestampAssigner<VisitorStats>() {
            @Override
            public long extractTimestamp(VisitorStats visitorStats, long l) {
                return visitorStats.getTs();
            }
        }));

        // 根据维度分组
        KeyedStream<VisitorStats, Tuple4<String, String, String, String>> vsKeyByDs = visitorStatsSingleOutputStreamOperator.keyBy(new KeySelector<VisitorStats, Tuple4<String, String, String, String>>() {

            @Override
            public Tuple4<String, String, String, String> getKey(VisitorStats v) throws Exception {
                return new Tuple4<>(v.getVc(), v.getCh(), v.getAr(), v.getIs_new());
            }
        });
        // 开窗聚合
        SingleOutputStreamOperator<VisitorStats> reduce = vsKeyByDs.window(TumblingEventTimeWindows.of(Time.seconds(10L)))
                .reduce(new ReduceFunction<VisitorStats>() {
                    @Override
                    public VisitorStats reduce(VisitorStats v1, VisitorStats v2) throws Exception {
                        v1.setPv_ct(v1.getPv_ct() + v2.getPv_ct());
                        v1.setSv_ct(v1.getSv_ct() + v2.getSv_ct());
                        v1.setUv_ct(v1.getUv_ct() + v2.getUv_ct());
                        v1.setUj_ct(v1.getUj_ct() + v2.getUj_ct());
                        return v1;
                    }
                }, new WindowFunction<VisitorStats, VisitorStats, Tuple4<String, String, String, String>, TimeWindow>() {
                    @Override
                    public void apply(Tuple4<String, String, String, String> stringStringStringStringTuple4, TimeWindow window, Iterable<VisitorStats> input, Collector<VisitorStats> out) throws Exception {
                        // 此种用法后续串口函数只对reduce结果进行了收集
                        VisitorStats visitorStats = input.iterator().next();
                        visitorStats.setStt(String.valueOf(window.getStart()));
                        visitorStats.setEdt(String.valueOf(window.getEnd()));
                        out.collect(visitorStats);
                    }
                });

        reduce.print();

        // 数据写入clickhouse
        reduce.addSink(ClickHouseUtil.getClickSink("insert into visitor_stats_2021 values(?,?,?,?,?,?,?,?,?,?,?,?)"));

        env.execute();
    }
}
