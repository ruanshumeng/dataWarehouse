package com.ruanshumeng.app.dws;

import com.ruanshumeng.bean.KeywordStats;
import com.ruanshumeng.common.GmallConstant;
import com.ruanshumeng.function.KeywordFunction;
import com.ruanshumeng.utils.ClickHouseUtil;
import com.ruanshumeng.utils.MyKafkaUtils;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 15:14 2022/1/18
 * @Modified By:
 */
public class KeywordStatsApp {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

      //TODO 2.注册自定义函数
        tableEnv.createTemporarySystemFunction("ik_analyze", KeywordFunction.class);
      //TODO 3.将数据源定义为动态表
        String groupId = "keyword_stats_app";
        String pageViewSourceTopic = "dwd_page_log";
        tableEnv.executeSql("CREATE TABLE page_view " +
                "(common MAP<STRING,STRING>, " +
                "page MAP<STRING,STRING>,ts BIGINT, " +
                "rowtime AS TO_TIMESTAMP(FROM_UNIXTIME(ts/1000, 'yyyy-MM-dd HH:mm:ss')) ," +
                "WATERMARK FOR rowtime AS rowtime - INTERVAL '2' SECOND) " +
                "WITH (" + MyKafkaUtils.getKafkaDDL(pageViewSourceTopic, groupId) + ")");

        // 过滤数据
        Table fullwordView = tableEnv.sqlQuery("select page['item'] fullword ," +
                "rowtime from page_view " +
                "where page['page_id']='good_list' " +
                "and page['item'] IS NOT NULL ");
        // 聚合
        Table keywordView = tableEnv.sqlQuery("select keyword,rowtime from " + fullwordView + " ," +
                " LATERAL TABLE(ik_analyze(fullword)) as T(keyword)");
        //TODO 6.根据各个关键词出现次数进行 ct
        Table keywordStatsSearch = tableEnv.sqlQuery("select keyword,count(*) ct, '"
                        + GmallConstant.KEYWORD_SEARCH + "' source ," +
                        "DATE_FORMAT(TUMBLE_START(rowtime, INTERVAL '10' SECOND),'yyyy-MM-dd HH:mm:ss')  stt," +
                "DATE_FORMAT(TUMBLE_END(rowtime, INTERVAL '10' SECOND),'yyyy-MM-dd HH:mm:ss')  edt," +
                "UNIX_TIMESTAMP()*1000 ts from "+keywordView   + " GROUP BY TUMBLE(rowtime, INTERVAL '10' SECOND ),keyword");

        //TODO 7.转换为数据流
        DataStream<KeywordStats> keywordStatsSearchDataStream =
                tableEnv.toAppendStream(keywordStatsSearch, KeywordStats.class);
        keywordStatsSearchDataStream.print();
        //TODO 8.写入到 ClickHouse
        keywordStatsSearchDataStream.addSink(ClickHouseUtil.getClickSink("insert into keyword_stats(keyword,ct,source,stt,edt,ts) values(?,?,?,?,?,?)"));

        env.execute();
    }
}
