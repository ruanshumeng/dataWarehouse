package com.ruanshumeng.app.serializer;

import com.alibaba.fastjson.JSONObject;
import com.ververica.cdc.debezium.DebeziumDeserializationSchema;
import io.debezium.data.Envelope;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.util.Collector;
import org.apache.kafka.connect.data.Field;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.source.SourceRecord;

import java.util.Date;
import java.util.List;

public class MyDeserializa implements DebeziumDeserializationSchema<String> {
    @Override
    public void deserialize(SourceRecord sourceRecord, Collector<String> collector) throws Exception {
        /**
         * SourceRecord{sourcePartition={server=mysql_binlog_source},
         * sourceOffset={file=mysql-bin.000010, pos=16881997, gtids=b53d75d9-de77-11e8-979d-7cd30adfbbf0:1-10256389,c6b5895c-de77-11e8-b189-7cd30ae40f92:1-212828795}}
         * ConnectRecord{topic='mysql_binlog_source.monitor_tk.test', kafkaPartition=null, key=null, keySchema=null, value=Struct{after=Struct{url=ww.test,status=2},
         * source=Struct{version=1.4.1.Final,connector=mysql,name=mysql_binlog_source,ts_ms=0,snapshot=last,db=monitor_tk,table=test,server_id=0,file=mysql-bin.000010,pos=16881997,row=0},
         * op=c,ts_ms=1633742311323}, valueSchema=Schema{mysql_binlog_source.monitor_tk.test.Envelope:STRUCT},
         * timestamp=null, headers=ConnectHeaders(headers=)}
         */
        JSONObject result = new JSONObject();

        // 获取表名
        String topic = sourceRecord.topic();
        String[] split = topic.split("\\.");
        result.put("tableName", split[2]);

        // 获取操作类型
        String code = Envelope.operationFor(sourceRecord).code();
        //
        switch (code) {
            case "d":
                result.put("type", "delete");
                break;
            case "u":
                result.put("type", "update");
                break;
            default:
                result.put("type", "insert");
        }

//        result.put("op", code);
//        if (!"c".equals(code)) return;

        // 获取数据
        Struct value = (Struct) sourceRecord.value();
        Struct before = value.getStruct("before");
        Struct after = value.getStruct("after");
        if (before != null) {
            List<Field> fields = before.schema().fields();
            JSONObject brforeJson = new JSONObject();
            for (Field field : fields) {
                brforeJson.put(field.name(), before.get(field.name()));
            }
            result.put("before", brforeJson);
        }
        if (after != null) {
            List<Field> fields = after.schema().fields();
            JSONObject afterJson = new JSONObject();
            for (Field field : fields) {
                afterJson.put(field.name(), after.get(field.name()));
            }
            result.put("after", afterJson);
        }

//        result.put("longTime", new Date().getTime());

        collector.collect(result.toJSONString());

    }

    @Override
    public TypeInformation<String> getProducedType() {
        return BasicTypeInfo.STRING_TYPE_INFO;
    }
}
