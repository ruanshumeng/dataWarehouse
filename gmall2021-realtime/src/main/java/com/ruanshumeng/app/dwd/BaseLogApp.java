package com.ruanshumeng.app.dwd;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.utils.MyKafkaUtils;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

/**
 * @Author:admin
 * @Description: 消费kafka中的日志数据
 * @Date:Created in 9:24 2021/11/26
 * 1. 获取kafka数据
 * 2. 转换成json
 * 3. 新老用户校验
 * 4. 分流
 * 5. 输出到其他topic主题中
 */


public class BaseLogApp {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        DataStreamSource<String> baseLog = env.addSource(MyKafkaUtils.getKafkaConsumer("ods_base_log", "ods_base_log_app1"));

        SingleOutputStreamOperator<JSONObject> jsonDS = baseLog.map(log -> JSONObject.parseObject(log));

        KeyedStream<JSONObject, String> keyedStream = jsonDS.keyBy(json -> json.getJSONObject("common").getString("mid"));

        SingleOutputStreamOperator<JSONObject> map = keyedStream.map(new RichMapFunction<JSONObject, JSONObject>() {

            // 定义状态检测是否为新用户
            ValueState<String> isNewState;

            @Override
            public void open(Configuration parameters) throws Exception {
                isNewState = getRuntimeContext().getState(new ValueStateDescriptor<String>("is_new_state", String.class));
            }

            @Override
            public JSONObject map(JSONObject value) throws Exception {
                String isNew = value.getJSONObject("common").getString("is_new");

                if ("1".equals(isNew)) {

                    if (isNewState.value() != null) {
                        value.getJSONObject("common").put("is_new", "0");
                    } else {
                        isNewState.update("0");
                    }

                }

                return value;
            }

        });


        /**
         * 1. 启动数据
         * 2. 页面数据
         * 3. 曝光数据（ 特殊的页面数据）
         */

        OutputTag<String> startOutPut = new OutputTag<String>("startOutPut") {
        };

        OutputTag<String> displayOutPut = new OutputTag<String>("displayOutPut") {
        };

        SingleOutputStreamOperator<String> process = map.process(new ProcessFunction<JSONObject, String>() {
            @Override
            public void processElement(JSONObject value, Context ctx, Collector<String> out) throws Exception {

                String start = value.getString("start");

                if (start != null && start.length() > 0) {
                    ctx.output(startOutPut, value.toJSONString());
                } else {
                    out.collect(value.toJSONString());

                    // 获取曝光日志
                    JSONArray jsonArray = value.getJSONArray("displays");

                    if (jsonArray != null && !jsonArray.isEmpty()) {
                        for (Object o : jsonArray) {
                            JSONObject json = (JSONObject) o;
                            json.put("page_id", value.getJSONObject("page").getString("page_id"));
                            ctx.output(displayOutPut, json.toJSONString());
                        }
                    }

                }


            }
        });




        process.getSideOutput(startOutPut).addSink(MyKafkaUtils.getKafkaProducer("dwd_start_log"));
        process.getSideOutput(displayOutPut).addSink(MyKafkaUtils.getKafkaProducer("dwd_display_log"));
        process.addSink(MyKafkaUtils.getKafkaProducer("dwd_page_log"));
        process.print("===============>");


        env.execute();

    }
}
