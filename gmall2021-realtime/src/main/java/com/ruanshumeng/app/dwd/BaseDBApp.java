package com.ruanshumeng.app.dwd;

import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.app.serializer.MyDeserializa;
import com.ruanshumeng.bean.TableProcess;
import com.ruanshumeng.function.HbaseSinkFunction;
import com.ruanshumeng.function.TableProcessFunction;
import com.ruanshumeng.utils.MyKafkaUtils;
import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.ververica.cdc.connectors.mysql.table.StartupOptions;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.streaming.api.functions.co.CoProcessFunction;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.streaming.api.functions.co.KeyedCoProcessFunction;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Arrays;
import java.util.List;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 13:58 2021/11/29
 * @Modified By:
 */
// mysql -> kafka(ods) ->kafka(dwd)/hbase(dim)
public class BaseDBApp {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(10000L);
        DataStreamSource<String> odsBaseDBSource = env.addSource(MyKafkaUtils.getKafkaConsumer("ods_base_db_22_1", "base_db_app"));


        SingleOutputStreamOperator<JSONObject> jsonDs = odsBaseDBSource.map(JSONObject::parseObject)
                .filter(json -> {
                    // TODO 过滤删除数据 ???
                    if ("delete".equals(json.getString("type"))) return false;
                    return true;
                });


        // 获取配置表的信息
        MySqlSource<String> source = MySqlSource.<String>builder()
                .hostname("119.91.199.53")
                .port(3306)
                .databaseList("gmall_realtime")
                .tableList("gmall_realtime.table_process")
                .username("root")
                .password("123456")
                .deserializer(new MyDeserializa())
                .startupOptions(StartupOptions.initial())
                .build();


        DataStreamSource<String> streamSource = env.fromSource(source, WatermarkStrategy.noWatermarks(), "realTime");
//        streamSource.print();
//        jsonDs.print();

        MapStateDescriptor<String, TableProcess> mapStateDescriptor = new MapStateDescriptor<String, TableProcess>("map-state", String.class, TableProcess.class);
        // 将配置表广播出去
        BroadcastStream<String> broadcast = streamSource.broadcast(mapStateDescriptor);

        // 定义侧输出流存放状态
        OutputTag<JSONObject> outputTag = new OutputTag<JSONObject>("hbase-output") {
        };

        // 连接两条流
        SingleOutputStreamOperator<JSONObject> process = jsonDs.connect(broadcast)
                .process(new TableProcessFunction(mapStateDescriptor,outputTag));


        process.print("kafka===>");
        DataStream<JSONObject> hbase = process.getSideOutput(outputTag);

        // 数据写入hbase
        hbase.addSink(new HbaseSinkFunction());
        // 数据写入kafka
        process.addSink(MyKafkaUtils.getKafkaProducer(new KafkaSerializationSchema<JSONObject>() {
            @Override
            public ProducerRecord<byte[], byte[]> serialize(JSONObject element, Long timestamp) {
                String topic = element.getString("sinkTable");
                JSONObject afterJson = element.getJSONObject("after");
                return new ProducerRecord<byte[], byte[]>(
                        topic,
                        afterJson.toJSONString().getBytes()
                );
            }
        }));



        env.execute();


    }
}
