package com.ruanshumeng.app.dwd;

import java.sql.*;

public class TestPhoenix {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {


        // 驱动
        String driver = "org.apache.phoenix.jdbc.PhoenixDriver";
        // 链接
        String url = "jdbc:phoenix:hadoop102,hadoop103,hadoop104:2181";

        // 加载驱动
        Class.forName(driver);

        // 创建连接
        Connection connection = DriverManager.getConnection(url);

        String tabName = "test5";

        //预编译sql
        PreparedStatement preparedStatement = connection.prepareStatement("select * from \"test5\"" );

        ResultSet resultSet = preparedStatement.executeQuery();


        while (resultSet.next()){
            System.out.println(resultSet.getString(1));
            System.out.println(resultSet.getString(2));
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();


    }
}
