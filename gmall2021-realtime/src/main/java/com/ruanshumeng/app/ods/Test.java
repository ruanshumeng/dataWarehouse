package com.ruanshumeng.app.ods;

import com.ruanshumeng.app.serializer.MyDeserializa;
import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Test {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(10000L);
        // MySqlSource
        MySqlSource<String> mysqlSource = MySqlSource.<String>builder()
                .hostname("123.123.123.123")
                .port(3306)
                .databaseList("gmall2021") // set captured database
                .tableList() // set captured table
                .username("root")
                .password("123456")
                .deserializer(new MyDeserializa()) // converts SourceRecord to JSON String
//                .includeSchemaChanges(false)
                .build();


        env.fromSource(mysqlSource, WatermarkStrategy.noWatermarks(), "MySQL Source")
                .print();

        env.execute();
    }
}
