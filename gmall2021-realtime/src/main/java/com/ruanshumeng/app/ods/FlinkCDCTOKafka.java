package com.ruanshumeng.app.ods;

import com.ruanshumeng.app.serializer.MyDeserializa;
import com.ruanshumeng.utils.MyKafkaUtils;
import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.ververica.cdc.connectors.mysql.table.StartupOptions;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;

// mysql -> kafka(ods)
public class FlinkCDCTOKafka {
    public static void main(String[] args) throws Exception {

        // 构建环境

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(10000L);

        MySqlSource<String> source = MySqlSource.<String>builder()
                .hostname("localhost")
                .port(3306)
                .databaseList("gmall2021")
                .tableList()
                .username("root")
                .password("123456")
                .deserializer(new MyDeserializa())
//                .startupOptions(StartupOptions.initial())
                .startupOptions(StartupOptions.latest())
                .build();



        DataStreamSource<String> mysqlCdcSource = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Mysql-cdc");
        mysqlCdcSource.print();
//
//
        mysqlCdcSource.addSink(MyKafkaUtils.getKafkaProducer("ods_base_db_22_1"));


        env.execute();



    }
}
