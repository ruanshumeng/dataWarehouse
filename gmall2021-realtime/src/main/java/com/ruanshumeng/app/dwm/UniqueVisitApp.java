package com.ruanshumeng.app.dwm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONAware;
import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.utils.MyKafkaUtils;
import org.apache.flink.api.common.functions.RichFilterFunction;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.StringUtils;

import java.text.SimpleDateFormat;

/**
 * @Author:admin
 * @Description: 根据mid 取出用户每天第一次登录 当作uv值
 * @Date:Created in 9:03 2021/12/15
 */
public class UniqueVisitApp {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(10000L);

        // {"common":{"ar":"440000","ba":"iPhone","ch":"Appstore","is_new":"0","md":"iPhone X","mid":"mid_112","os":"iOS 13.2.9","uid":"18","vc":"v2.1.134"},
        // "page":{"during_time":18848,"item":"9,10","item_type":"sku_ids","last_page_id":"trade","page_id":"payment"},
        // "ts":1633761692000}
        DataStreamSource<String> pageLog = env.addSource(MyKafkaUtils.getKafkaConsumer("dwd_page_log", "base_db_app"));

        SingleOutputStreamOperator<JSONObject> filter = pageLog.map(JSON::parseObject)
                // 根据mid进行分组
                .keyBy(json -> json.getJSONObject("common").getString("mid"))
                // 过滤掉多余的登录次数
                .filter(new RichFilterFunction<JSONObject>() {

                    // 定义sdf格式化时间戳 存放数据为yyyy/MM/dd
                    SimpleDateFormat sdf;
                    ValueState<String> valueState;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        sdf = new SimpleDateFormat("yyyy/MM/dd");

                        ValueStateDescriptor<String> stateDescriptor = new ValueStateDescriptor<>("mid-state", String.class);
                        // 设置状态存活时间 过期时间一天 每次过期会重新设置过期时间
                        StateTtlConfig build = StateTtlConfig.newBuilder(Time.days(1))
                                .setUpdateType(StateTtlConfig.UpdateType.OnCreateAndWrite).build();
                        stateDescriptor.enableTimeToLive(build);
                        valueState = getRuntimeContext().getState(stateDescriptor);
                    }

                    // 只有退出后重新访问的才计算
                    @Override
                    public boolean filter(JSONObject value) throws Exception {

                        String lastPageId = value.getJSONObject("page").getString("last_page_id");
                        // 如果为空或者空字符串代表用户第一次登录 反之
                        if (!StringUtils.isNullOrWhitespaceOnly(lastPageId)) return false;

                        String formatDate = sdf.format(value.getLong("ts"));

                        String stateValue = valueState.value();
                        if (stateValue == null || !stateValue.equals(formatDate)) {
                            valueState.update(formatDate);
                            return true;
                        }
                        return false;
                    }
                });

        filter.map(JSONAware::toJSONString).addSink(MyKafkaUtils.getKafkaProducer("dwm_unique_visit"));
        env.execute();


    }
}
