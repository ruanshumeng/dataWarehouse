package com.ruanshumeng.app.dwm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONAware;
import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.utils.MyKafkaUtils;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.cep.CEP;
import org.apache.flink.cep.PatternSelectFunction;
import org.apache.flink.cep.PatternStream;
import org.apache.flink.cep.PatternTimeoutFunction;
import org.apache.flink.cep.pattern.Pattern;
import org.apache.flink.cep.pattern.conditions.SimpleCondition;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.OutputTag;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class UserJumpDetailApp {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(10000L);

        // {"common":{"ar":"440000","ba":"iPhone","ch":"Appstore","is_new":"0","md":"iPhone X","mid":"mid_112","os":"iOS 13.2.9","uid":"18","vc":"v2.1.134"},
        // "page":{"during_time":18848,"item":"9,10","item_type":"sku_ids","last_page_id":"trade","page_id":"payment"},
        // "ts":1633761692000}
        DataStreamSource<String> pageLog = env.addSource(MyKafkaUtils.getKafkaConsumer("dwd_page_log", "base_db_app"));

        pageLog.print("===============》");

        SingleOutputStreamOperator<JSONObject> streamOperator = pageLog.map(JSON::parseObject)
                .assignTimestampsAndWatermarks(WatermarkStrategy
                        .<JSONObject>forBoundedOutOfOrderness(Duration.ofSeconds(1L)
                        ).withTimestampAssigner(new SerializableTimestampAssigner<JSONObject>() {
                            @Override
                            public long extractTimestamp(JSONObject jsonObject, long l) {
                                return jsonObject.getLong("ts");
                            }
                        }));


        // 定义模式序列
//        Pattern<JSONObject, JSONObject> pattern = Pattern.<JSONObject>begin("start")
//                .where(new SimpleCondition<JSONObject>() {
//                    @Override
//                    public boolean filter(JSONObject jsonObject) throws Exception {
//                        String lastPage = jsonObject.getJSONObject("page").getString("last_page_id");
//                        return lastPage == null || lastPage.length() <= 0;
//                    }
//                }).next("end")
//                .where(new SimpleCondition<JSONObject>() {
//                    @Override
//                    public boolean filter(JSONObject jsonObject) throws Exception {
//                        String lastPage = jsonObject.getJSONObject("page").getString("last_page_id");
//                        return lastPage == null || lastPage.length() <= 0;
//                    }
//                })
//                .within(Time.seconds(10));


        Pattern<JSONObject, JSONObject> pattern = Pattern.<JSONObject>begin("start")
                .where(new SimpleCondition<JSONObject>() {
                    @Override
                    public boolean filter(JSONObject jsonObject) throws Exception {
                        String lastPage = jsonObject.getJSONObject("page").getString("last_page_id");
                        return lastPage == null || lastPage.length() <= 0;
                    }
                }).times(2)
                .consecutive() // 严格近邻模式  默认为宽松
                .within(Time.seconds(10));


        PatternStream<JSONObject> patternStream = CEP
                .pattern(streamOperator
                        .keyBy(jsonObject -> jsonObject.getJSONObject("common").getString("mid")
                        ), pattern);

        OutputTag<JSONObject> outputTag = new OutputTag<JSONObject>("out-state") {
        };

        SingleOutputStreamOperator<JSONObject> select = patternStream.select(outputTag,
                new PatternTimeoutFunction<JSONObject, JSONObject>() {
                    @Override
                    public JSONObject timeout(Map<String, List<JSONObject>> map, long l) throws Exception {
                        return map.get("start").get(0);
                    }
                },
                new PatternSelectFunction<JSONObject, JSONObject>() {
                    @Override
                    public JSONObject select(Map<String, List<JSONObject>> map) throws Exception {
                        return map.get("start").get(0);
                    }
                });

        DataStream<JSONObject> union = select.union(select.getSideOutput(outputTag));

        union.map(JSONAware::toJSONString).addSink(MyKafkaUtils.getKafkaProducer("dwm_jump_detail"));


        env.execute();
    }
}
