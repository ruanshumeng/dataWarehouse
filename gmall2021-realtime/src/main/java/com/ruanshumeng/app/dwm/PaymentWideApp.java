package com.ruanshumeng.app.dwm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.bean.OrderWide;
import com.ruanshumeng.bean.PaymentInfo;
import com.ruanshumeng.bean.PaymentWide;
import com.ruanshumeng.utils.MyKafkaUtils;
import lombok.SneakyThrows;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import java.text.SimpleDateFormat;

/**{"callback_time":"2021-12-23 22:36:50","payment_type":"1101","out_trade_no":"487796379642468","create_time":"2021-12-23 22:36:30","user_id":3891,"total_amount":4277.00,"subject":"华为 HUAWEI P40 麒麟990 5G SoC芯片 5000万超感知徕卡三摄 30倍数字变焦 6GB+128GB亮黑色全网通5G手机等2件商品","trade_no":"7783539142356515742689437329194389","id":20468,"order_id":30458}
 * @Author:admin
 * @Description:
 * @Date:Created in 14:51 2021/12/21
 * @Modified By:
 */
public class PaymentWideApp {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // 1. 获取数据kafka中的数据
        DataStreamSource<String> orderWideDS = env.addSource(MyKafkaUtils.getKafkaConsumer("dwm_order_wide", "order_wide_group3"));
        DataStreamSource<String> payMentInfoDS = env.addSource(MyKafkaUtils.getKafkaConsumer("dwd_payment_info", "order_wide_group3"));
        env.setParallelism(1);

        // 转换
        SingleOutputStreamOperator<OrderWide> orderWideMapDs = orderWideDS.map(orderWide -> JSON.parseObject(orderWide, OrderWide.class))
                .assignTimestampsAndWatermarks(WatermarkStrategy.<OrderWide>forMonotonousTimestamps()
                .withTimestampAssigner(new SerializableTimestampAssigner<OrderWide>() {
                    @SneakyThrows
                    @Override
                    public long extractTimestamp(OrderWide element, long recordTimestamp) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String create_time = element.getCreate_time();
                        long time = sdf.parse(create_time).getTime();
                        return time;
                    }
                }));
        SingleOutputStreamOperator<PaymentInfo> payMentMapDs = payMentInfoDS.map(pay -> JSON.parseObject(pay, PaymentInfo.class))
                .assignTimestampsAndWatermarks(WatermarkStrategy.<PaymentInfo>forMonotonousTimestamps()
                        .withTimestampAssigner(new SerializableTimestampAssigner<PaymentInfo>() {
                            @SneakyThrows
                            @Override
                            public long extractTimestamp(PaymentInfo element, long recordTimestamp) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String create_time = element.getCreate_time();
                                long time = sdf.parse(create_time).getTime();
                                return time;
                            }
                        }));

        // 双流join
        SingleOutputStreamOperator<PaymentWide> process = payMentMapDs.keyBy(PaymentInfo::getOrder_id)
                .intervalJoin(orderWideMapDs.keyBy(OrderWide::getOrder_id))
                .between(Time.minutes(-15), Time.seconds(5))
                .process(new ProcessJoinFunction<PaymentInfo, OrderWide, PaymentWide>() {
                    @Override
                    public void processElement(PaymentInfo paymentInfo, OrderWide orderWide, Context ctx, Collector<PaymentWide> out) throws Exception {
                        out.collect(new PaymentWide(paymentInfo, orderWide));
                    }
                });


        process.print();

        process.map(JSONObject::toJSONString).addSink(MyKafkaUtils.getKafkaProducer("dwm_pay_ment_info"));


        env.execute();
    }
}
