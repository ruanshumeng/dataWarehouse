package com.ruanshumeng.app.dwm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruanshumeng.bean.OrderDetail;
import com.ruanshumeng.bean.OrderInfo;
import com.ruanshumeng.bean.OrderWide;
import com.ruanshumeng.common.baseDateEnum;
import com.ruanshumeng.function.DimAsyncFunction;
import com.ruanshumeng.utils.MyKafkaUtils;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 13:42 2021/12/17
 * @Modified By:
 */
public class OrderWideApp {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        // 1. 获取数据kafka中的数据
        DataStreamSource<String> orderInfo = env.addSource(MyKafkaUtils.getKafkaConsumer("dwd_order_info", "order_wide_group2"));
        DataStreamSource<String> orderDetail = env.addSource(MyKafkaUtils.getKafkaConsumer("dwd_order_detail", "order_wide_group2"));

        // 2.转化成java Bean
        SingleOutputStreamOperator<OrderInfo> infoStream = orderInfo.map(json -> {
                    OrderInfo order = JSON.parseObject(json, OrderInfo.class);

                    String create_time = order.getCreate_time();
                    String[] time = create_time.split(" ");
                    order.setCreate_date(time[0]);
                    order.setCreate_hour(time[1].split(":")[0]);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:MM:ss");
                    long longTime = sdf.parse(create_time).getTime();
                    order.setCreate_ts(longTime);

                    return order;
                })
                .assignTimestampsAndWatermarks(WatermarkStrategy.<OrderInfo>forMonotonousTimestamps()
                        .withTimestampAssigner(new SerializableTimestampAssigner<OrderInfo>() {
                    @Override
                    public long extractTimestamp(OrderInfo element, long recordTimestamp) {
                        return element.getCreate_ts();
                    }
                }));

        SingleOutputStreamOperator<OrderDetail> detailStream = orderDetail.map(json -> {
            OrderDetail detail = JSON.parseObject(json, OrderDetail.class);
            String create_time = detail.getCreate_time();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:MM:ss");
            long longTime = sdf.parse(create_time).getTime();
            detail.setCreate_ts(longTime);
            return detail;
        }).assignTimestampsAndWatermarks(WatermarkStrategy.<OrderDetail>forMonotonousTimestamps()
                .withTimestampAssigner(new SerializableTimestampAssigner<OrderDetail>() {
                    @Override
                    public long extractTimestamp(OrderDetail orderDetail, long l) {
                        return orderDetail.getCreate_ts();
                    }
                }));
        // 双流join  生成宽表
        SingleOutputStreamOperator<OrderWide> process = infoStream.keyBy(OrderInfo::getId)
                .intervalJoin(detailStream.keyBy(OrderDetail::getOrder_id))
                .between(Time.seconds(-5), Time.seconds(5))
                .process(new ProcessJoinFunction<OrderInfo, OrderDetail, OrderWide>() {
                    @Override
                    public void processElement(OrderInfo info, OrderDetail detail, ProcessJoinFunction<OrderInfo, OrderDetail, OrderWide>.Context ctx, Collector<OrderWide> out) throws Exception {
                        out.collect(new OrderWide(info, detail));
                    }
                });

        process.print();
//
//        // 通过异步io的方式关联其他数据 关联用户
        SingleOutputStreamOperator<OrderWide> orderWideWithUserDS = AsyncDataStream.unorderedWait(process,
                new DimAsyncFunction<OrderWide>("DIM_USER_INFO") {

                    @Override
                    public String getKey(OrderWide input) {
                        return input.getUser_id().toString();
                    }

                    @Override
                    public void join(OrderWide orderWide, JSONObject dimInfo) throws ParseException {

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                        //取出用户维度中的生日
                        String birthday = dimInfo.getString("birthday");
                        long currentTS = System.currentTimeMillis();
                        Long ts = sdf.parse(birthday).getTime();

//                        //将生日字段处理成年纪
                        Long ageLong = (currentTS - ts) / 1000L / 60 / 60 / 24 / 365;
                        orderWide.setUser_age(ageLong.intValue());
                        //取出用户维度中的性别
                        String gender = dimInfo.getString("gender");
                        orderWide.setUser_gender(gender);
                    }
                }
                , 60,
                TimeUnit.SECONDS);
//
//        orderWideWithUserDS.print("orderWideWithUserDS");
//
//        orderWideWithUserDS.print("orderWideWithUserDS");
//
//          // 关联地区
        SingleOutputStreamOperator<OrderWide> orderWideWithProvinceDS = AsyncDataStream.unorderedWait(orderWideWithUserDS,
                new DimAsyncFunction<OrderWide>("DIM_BASE_PROVINCE") {

                    @Override
                    public String getKey(OrderWide orderWide) {
                        return orderWide.getProvince_id().toString();
                    }

                    @Override
                    public void join(OrderWide orderWide, JSONObject dimInfo) {
                        //提取维度信息并设置进orderWide
                        orderWide.setProvince_name(dimInfo.getString("name"));
                        orderWide.setProvince_area_code(dimInfo.getString("areaCode"));
                        orderWide.setProvince_iso_code(dimInfo.getString("isoCode"));
                        orderWide.setProvince_3166_2_code(dimInfo.getString("iso31662"));
                    }
                }, 60, TimeUnit.SECONDS);

//        orderWideWithProvinceDS.print("orderWideWithProvinceDS");
//        //5.3 关联SKU维度
        SingleOutputStreamOperator<OrderWide> orderWideWithSkuDS = AsyncDataStream.unorderedWait(
                orderWideWithProvinceDS, new DimAsyncFunction<OrderWide>("DIM_SKU_INFO") {
                    @Override
                    public void join(OrderWide orderWide, JSONObject jsonObject) {
                        orderWide.setSku_name(jsonObject.getString("skuName"));
                        orderWide.setCategory3_id(jsonObject.getLong("category3Id"));
                        orderWide.setSpu_id(jsonObject.getLong("spuId"));
                        orderWide.setTm_id(jsonObject.getLong("tmId"));
                    }

                    @Override
                    public String getKey(OrderWide orderWide) {
                        return String.valueOf(orderWide.getSku_id());
                    }
                }, 60, TimeUnit.SECONDS);
//        orderWideWithSkuDS.print("orderWideWithSkuDS");
//        //5.4 关联SPU维度
        SingleOutputStreamOperator<OrderWide> orderWideWithSpuDS = AsyncDataStream.unorderedWait(
                orderWideWithSkuDS, new DimAsyncFunction<OrderWide>("DIM_SPU_INFO") {
                    @Override
                    public void join(OrderWide orderWide, JSONObject jsonObject) {
                        orderWide.setSpu_name(jsonObject.getString("spuName"));
                    }

                    @Override
                    public String getKey(OrderWide orderWide) {
                        return String.valueOf(orderWide.getSpu_id());
                    }
                }, 60, TimeUnit.SECONDS);
//        orderWideWithSpuDS.print("orderWideWithSpuDS");
//        //5.5 关联品牌维度
        SingleOutputStreamOperator<OrderWide> orderWideWithTmDS = AsyncDataStream.unorderedWait(
                orderWideWithSpuDS, new DimAsyncFunction<OrderWide>("DIM_BASE_TRADEMARK") {
                    @Override
                    public void join(OrderWide orderWide, JSONObject jsonObject) {
                          orderWide.setTm_name(jsonObject.getString("tmName"));
                    }

                    @Override
                    public String getKey(OrderWide orderWide) {
                        return String.valueOf(orderWide.getTm_id());
                    }
                }, 60, TimeUnit.SECONDS);
        //5.6 关联品类维度
        SingleOutputStreamOperator<OrderWide> orderWideWithCategory3DS = AsyncDataStream.unorderedWait(
                orderWideWithTmDS, new DimAsyncFunction<OrderWide>("DIM_BASE_CATEGORY3") {
                    @Override
                    public void join(OrderWide orderWide, JSONObject jsonObject) {
                        orderWide.setCategory3_name(jsonObject.getString("name"));
                    }

                    @Override
                    public String getKey(OrderWide orderWide) {
                        return String.valueOf(orderWide.getCategory3_id());
                    }
                }, 60 , TimeUnit.SECONDS);

        orderWideWithCategory3DS.print("orderWideWithCategory3DS=====》");
//
//
        orderWideWithCategory3DS.map(JSONObject::toJSONString).addSink(MyKafkaUtils.getKafkaProducer("dwm_order_wide"));

        env.execute();

    }
}
