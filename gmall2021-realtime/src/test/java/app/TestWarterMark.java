package app;

import bean.Bean1;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

/**
 * @Author:admin
 * @Description:
 * @Date:Created in 14:09 2021/12/17
 * @Modified By:
 */
public class TestWarterMark {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> socketTextStream = env.socketTextStream("10.1.222.28", 7777);

        SingleOutputStreamOperator<Bean1> b1 = socketTextStream.map(s -> {
            String[] words = s.split(" ");
            Bean1 bean1 = new Bean1();
            bean1.setId(words[0]);
            bean1.setName(words[1]);
            bean1.setTs(Long.valueOf(words[2]));
            return bean1;
        }).assignTimestampsAndWatermarks(WatermarkStrategy.<Bean1>forBoundedOutOfOrderness(Duration.ofSeconds(2))
                .withTimestampAssigner(new SerializableTimestampAssigner<Bean1>() {
                    @Override
                    public long extractTimestamp(Bean1 bean1, long l) {
                        return bean1.getTs() * 1000;
                    }
                }));
// 开始时间 = timestamp - (timestamp - offset + windowSize) % windowSize
        // 10 - (10 - 2 + 5) % 5
        b1.keyBy(Bean1::getId)
                .window(SlidingEventTimeWindows.of(Time.seconds(5), Time.seconds(2)))
                .process(new ProcessWindowFunction<Bean1, Object, String, TimeWindow>() {
                    @Override
                    public void process(String s, Context context, Iterable<Bean1> elements, Collector<Object> out) throws Exception {
                        System.out.println("窗口开始" + context.window().getStart());
                        System.out.println("窗口结束" + context.window().getEnd());
                        System.out.println("数据总数" + elements.spliterator().estimateSize());
                    }
                });
//                .print();


        env.execute();


    }
}
