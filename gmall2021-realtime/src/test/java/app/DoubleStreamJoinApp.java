package app;

import bean.Bean1;
import bean.Bean2;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import java.time.Duration;

public class DoubleStreamJoinApp {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<String> socketTextStream = env.socketTextStream("10.1.222.28", 7777);
        DataStreamSource<String> socketTextStream2 = env.socketTextStream("10.1.222.28", 7788);

        SingleOutputStreamOperator<Bean1> b1 = socketTextStream.map(s -> {
            String[] words = s.split(" ");
            Bean1 bean1 = new Bean1();
            bean1.setId(words[0]);
            bean1.setName(words[1]);
            bean1.setTs(Long.valueOf(words[2]));
            return bean1;
        }).assignTimestampsAndWatermarks(WatermarkStrategy.<Bean1>forBoundedOutOfOrderness(Duration.ofSeconds(2))
                .withTimestampAssigner(new SerializableTimestampAssigner<Bean1>() {
                    @Override
                    public long extractTimestamp(Bean1 bean1, long l) {
                        return bean1.getTs() * 1000;
                    }
                }));


        SingleOutputStreamOperator<Bean2> b2 = socketTextStream2.map(s -> {
            String[] words = s.split(" ");
            Bean2 bean1 = new Bean2();
            bean1.setId(words[0]);
            bean1.setName(words[1]);
            bean1.setTs(Long.valueOf(words[2]));
            return bean1;
        }).assignTimestampsAndWatermarks(WatermarkStrategy.<Bean2>forBoundedOutOfOrderness(Duration.ofSeconds(2))
                .withTimestampAssigner(new SerializableTimestampAssigner<Bean2>() {
                    @Override
                    public long extractTimestamp(Bean2 bean1, long l) {
                        return bean1.getTs() * 1000;
                    }
                }));


        SingleOutputStreamOperator<String> process = b1.keyBy(b -> b.getId())
                .intervalJoin(b2.keyBy(b -> b.getId()))
                .between(Time.seconds(-5), Time.seconds(5))
                .process(new ProcessJoinFunction<Bean1, Bean2, String>() {
                    @Override
                    public void processElement(Bean1 left, Bean2 right, ProcessJoinFunction<Bean1, Bean2, String>.Context ctx, Collector<String> out) throws Exception {
                        out.collect(left.toString() + right.toString());
                    }
                });

        process.print();


        env.execute();
    }
}
